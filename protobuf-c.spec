%bcond_without scl

%if %{with scl}
%global scl isc-bind
%endif

# Work around an SCL build issue on Fedora 33+
# (https://bugzilla.redhat.com/show_bug.cgi?id=1898587)
%if 0%{?fedora} >= 33
%global __python %{__python3}
%endif

%{?scl:%scl_package protobuf-c}
%{!?scl:%global pkg_name %{name}}

Name:           %{?scl_prefix}protobuf-c
Version:        1.4.1
Release:        4%{?dist}
Summary:        C bindings for Google's Protocol Buffers

License:        BSD
URL:            https://github.com/%{pkg_name}/%{pkg_name}
Source0:        %{url}/releases/download/v%{version}/%{pkg_name}-%{version}.tar.gz

BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  gcc-c++
BuildRequires:  make
BuildRequires:  pkgconfig(protobuf)
%if 0%{?rhel} && 0%{?rhel} < 8
BuildRequires:  devtoolset-8
%endif

%{?scl:BuildRequires: %{scl}-build}
%{?scl:BuildRequires: %scl_runtime}
%{?scl:Requires: %scl_runtime}

%description
Protocol Buffers are a way of encoding structured data in an efficient yet
extensible format. This package provides a code generator and run-time
libraries to use Protocol Buffers from pure C (not C++).

It uses a modified version of protoc called protoc-c.

%package compiler
Summary:        Protocol Buffers C compiler
Requires:       %{name}%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}

%description compiler
This package contains a modified version of the Protocol Buffers
compiler for the C programming language called protoc-c.

%package devel
Summary:        Protocol Buffers C headers and libraries
Requires:       %{name}%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Requires:       %{name}-compiler%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}

%description devel
This package contains protobuf-c headers and libraries.

%prep
%autosetup -n %{pkg_name}-%{version} -p1

%build
%if 0%{?rhel} && 0%{?rhel} < 8
scl enable devtoolset-8 -- <<\EOF
%endif
# https://github.com/protobuf-c/protobuf-c/issues/483
%set_build_flags
export CXXFLAGS="${CXXFLAGS} -std=c++17"
%configure --disable-static
%{?scl:scl enable %scl -- }%make_build
%if 0%{?rhel} && 0%{?rhel} < 8
EOF
%endif

%check
%if 0%{?rhel} && 0%{?rhel} < 8
scl enable devtoolset-8 -- <<\EOF
%endif
make check
%if 0%{?rhel} && 0%{?rhel} < 8
EOF
%endif

%install
%make_install
find %{buildroot} -type f -name '*.la' -delete

%files
%license LICENSE
%doc ChangeLog README.md TODO
%{_libdir}/lib%{pkg_name}.so.*

%files compiler
%{_bindir}/protoc-c
%{_bindir}/protoc-gen-c

%files devel
%dir %{_includedir}/google
%{_includedir}/%{pkg_name}/
%{_includedir}/google/%{pkg_name}/
%{_libdir}/lib%{pkg_name}.so
%{_libdir}/pkgconfig/lib%{pkg_name}.pc

%changelog
